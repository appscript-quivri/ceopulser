import { Component, OnInit } from '@angular/core';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-player',
  templateUrl: './player.page.html',
  styleUrls: ['./player.page.scss'], 
})
export class PlayerPage implements OnInit {
    file: MediaObject;
    playIcon = 'pause';
    audioDuration: number;
    currentPosition: number;
    restTime: string;

  constructor(
      public platform: Platform,
      public media: Media,
      ) {
          this.platform.ready().then(() => {
              this.readAudio(); 
          });
      }

  ngOnInit() {
  }
  
    
    convertSec(secondes: number) {
      const min = Math.floor((secondes/60/60)*60);
      const sec = Math.floor(((secondes/60/60)*60 - min)*60);
      this.restTime = min + 'm ' + sec + 's';
    }
    
    
  
  playPause() {
      if(this.playIcon == 'pause') {
        this.playIcon = 'play';
        this.file.pause();
      } else {
        this.playIcon = 'pause'; 
        this.file.play();
      }
    }
  
  readAudio() { 
      this.file = this.media.create('https://ia801407.us.archive.org/26/items/erothyme-cherry-picking/Erothyme-CherryPicking.mp3');
      // to listen to plugin events:
      this.file.onStatusUpdate.subscribe(status => console.log(status)); // fires when file status changes
      this.file.onSuccess.subscribe(() => console.log('Action is successful'));
      this.file.onError.subscribe(error => console.log('Error!', error));
      // play the file
      this.file.play();
      // get current position
      setInterval(() => {
        this.file.getCurrentPosition().then((position) => {
          this.audioDuration = Math.floor(this.file.getDuration());
          this.currentPosition = Math.floor(position);
          this.convertSec(this.audioDuration - this.currentPosition);
        });
      }, 1000 );
    }
    
    
    changePosition() {
          console.log('changePosition: ' + this.currentPosition);
          this.file.seekTo(this.currentPosition*1000);
          this.convertSec(this.audioDuration - this.currentPosition);
        }
        
        
    seekTo(duration: number) {
      console.log('décalage de ' + duration / 1000 + ' secondes');
      this.file.seekTo(this.currentPosition*1000 + duration);
      this.currentPosition = this.currentPosition + Math.floor(duration/1000);
      this.convertSec(this.audioDuration - this.currentPosition);
    }        

}
