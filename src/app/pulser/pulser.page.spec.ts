import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PulserPage } from './pulser.page';

describe('PulserPage', () => {
  let component: PulserPage;
  let fixture: ComponentFixture<PulserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PulserPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PulserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
